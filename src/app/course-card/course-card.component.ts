import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Course} from '../model/course';

@Component({
  selector: 'course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent implements OnInit{
  @Input({required: true}) course: Course
  @Input({required: true}) courseIndex: number

  @Output()
  courseSelected = new EventEmitter<Course>()

  ngOnInit() {
  }

  isImageVisible() {
    return this.course && this.course.iconUrl;
  }

  onCourseViewed() {
    console.log("course viewed");

    this.courseSelected.emit(this.course)
  }

  setClasses() {
    return {
      'beginner': this.course.category === 'BEGINNER'
    }
  }

}
