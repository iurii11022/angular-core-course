import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CourseCardComponent } from './course-card/course-card.component';
import { CourseCardNgTamplateComponent } from './course-card-ng-tamplate/course-card-ng-tamplate.component';

@NgModule({
  declarations: [
    AppComponent,
    CourseCardComponent,
    CourseCardNgTamplateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
