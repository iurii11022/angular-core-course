import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Course} from '../model/course';

@Component({
  selector: 'course-card-ng-tamplate',
  templateUrl: './course-card-ng-tamplate.component.html',
  styleUrls: ['./course-card-ng-tamplate.component.css']
})
export class CourseCardNgTamplateComponent {
  @Input({required: true}) course: Course
  @Input({required: true}) courseIndex: number

  @Output()
  courseSelected = new EventEmitter<Course>()

  ngOnInit() {
  }

  isImageVisible() {
    return this.course && this.course.iconUrl;
  }

  onCourseViewed() {
    console.log("course viewed");

    this.courseSelected.emit(this.course)
  }

  setClasses() {
    return {
      'beginner': this.course.category === 'BEGINNER'
    }
  }
}
