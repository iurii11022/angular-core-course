import { Component } from '@angular/core';
import {COURSES} from '../db-data';
import {Course} from './model/course';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  startDate= new Date()

  course1 = COURSES[0]

  courses = COURSES;

  coreCourse = COURSES[0];

  rxJSCourse = COURSES[1];

  ndRxCourse = COURSES[2]

  onCourseSelected(selectedCourse: Course) {
    console.log("app-component clicked", selectedCourse);
  }
}
